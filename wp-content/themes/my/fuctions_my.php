<?php 
function themename_custom_logo_setup() {
		$defaults = array(
			'height'      => 100,
			'width'       => 400,
			'flex-height' => true,
			'flex-width'  => true,
			'header-text' => array( 'site-title', 'site-description' ),
		);
		add_theme_support( 'custom-logo', $defaults );
	}
	add_action( 'after_setup_theme', 'themename_custom_logo_setup' );
	function create_post_type() {
		register_post_type( 'acme_product',
		  array(
			'labels' => array(
			  'name' => __( 'Contact details' ),
			  'singular_name' => __( 'contact_ide' )
			),
			'public' => true,
			'has_archive' => true,
		  )
		);
	  }
	  add_action( 'init', 'create_post_type' );
	  // Our custom post type function
	function create_posttype() {
	
		register_post_type( 'movies',
		// CPT Options
			array(
				'labels' => array(
					'name' => __( 'Movies' ),
					'singular_name' => __( 'Movie' )
				),
				'public' => true,
				'has_archive' => true,
				'rewrite' => array('slug' => 'movies'),
			)
		);
	} 
	// Hooking up our function to theme setup
	add_action( 'init', 'create_posttype' ); ?>