<?php /* Template Name: About */ ?>
<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<?php $top_post = get_post('117'); ?>
<div class="header-top"></div>
<div class="container-fluid" id="gallery" style="background: url('<?php echo get_the_post_thumbnail_url( '117' ); ?>')">
    <div class="title">
        <h1><?php echo $top_post->post_title; ?></h1>
    </div>
</div>
<div class="container-fluid">
    <div class="row" id="top_gallery">
        <?php  echo $top_post->post_content;?>
    </div>
</div>

<?php 
$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>
<?php if ( $wpb_all_query->have_posts() ) : ?>
<div class="container-fluid" id="gallery_posts">
    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
    <div class="row padding-top-high">
        <div class="title col-md-4">
            <h2><?php the_title(); ?></h2>
            <div class="descript">
            <?php if (has_excerpt()) {
                the_excerpt();
            } else {
                echo "";
            } ?>
            </div>
        </div>
        <div class="content col-md-8">
            <?php the_content(); ?>
        </div>
        <div class="layer-cut"></div>
    </div>
    <?php endwhile; ?>
</div>
    <?php wp_reset_postdata(); ?>
 
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<?php get_footer(); ?>
