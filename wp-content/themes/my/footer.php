<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

</div><!-- .site-content -->
<footer id="footer" >
	<div class="padding-top padding-bottom container-fluid">
		<h1>
			<?php _e("CERTIFIED QUALITY AND GUARANTEED WORKS"); ?>
		</h1>
	</div>
	<div class="container-fluid" id="main_footer">
		<div class="row padding-top padding-bottom ">
			<?php $args = array( 'post_type' => 'acme_product', 'posts_per_page' => 3 );
				$count_post = 0;
				$the_query = new WP_Query( $args ); 
				while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<div class="col-sm-4 padding-bottom-small padding-top-small">
						<div class="margin_midle_div">
							
								<?php if($the_query->current_post==0) : ?><a href="tel:<?php echo get_the_title(); ?>"><?php $count_post++ ?><?php endif; ?>
								<?php if($the_query->current_post==1) : ?><a href="mailto:<?php echo get_the_title(); ?>"><?php $count_post++ ?><?php endif; ?>
								<?php if($the_query->current_post==2) : ?><a href="http://maps.google.com/?q=<?php echo get_the_title(); ?>" target="_blank"><?php $count_post++ ?><?php endif; ?>
									<img src="<?php echo get_the_excerpt(); ?>" />
									<h5><?php echo get_the_title(); ?></h5>
								</a>
							
						</div>
					</div>
				<?php endwhile; ?>
			<?php  wp_reset_postdata(); ?>
		</div>
	</div>
	<div class="credits">
		<p>&copy; Tenerife furniture and home renovation 2018</p>
		<p style="text-align: center; font-size: 13px;">Design made by <a href="http://biggabba.com" target="_blank">http://biggabba.com</a></p>
		<div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
	</div>
</footer><!-- .site-footer -->

</div><!-- .site -->
<?php // MENU CONTACT FORM ?>
<div id="menu_contact">
	<div id="menu_close">X</div>
	<div class="row padding-top padding-bottom ">
		<?php $args = array( 'post_type' => 'acme_product', 'posts_per_page' => 3 );
			$count_post = 0;
			$the_query = new WP_Query( $args ); 
			while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<div class="col-sm-4 padding-bottom-small padding-top-small">
					<div class="margin_midle_div">
						<?php if($the_query->current_post==0) : ?><a href="tel:<?php echo get_the_title(); ?>"><?php $count_post++ ?><?php endif; ?>
						<?php if($the_query->current_post==1) : ?><a href="mailto:<?php echo get_the_title(); ?>"><?php $count_post++ ?><?php endif; ?>
						<?php if($the_query->current_post==2) : ?><a href="http://maps.google.com/?q=<?php echo get_the_title(); ?>" target="_blank"><?php $count_post++ ?><?php endif; ?>
							<img src="<?php echo get_the_excerpt(); ?>" />
							<h5><?php echo get_the_title(); ?></h5>
						</a>
					</div>
				</div>
			<?php endwhile; ?>
		<?php  wp_reset_postdata(); ?>
	</div>
	<div class="row">
		<?php echo do_shortcode( '[contact-form-7 id="114" title="Contact form 1"]' ); ?>
	</div>
</div>
<?php //wp_footer(); ?>
<script src="<?php bloginfo('template_directory');?>/js/jquery-3.3.1.min.js"></script>
<script src="<?php bloginfo('template_directory');?>/js/jquery.flexslider.js"></script>
<script src="<?php bloginfo('template_directory');?>/js/slick.min.js"></script>
<script src="<?php bloginfo('template_directory');?>/js/my_js.js"></script>
<script src="<?php bloginfo('template_directory');?>/js/lightgallery.js"></script>
<script>
	<?php
		$taxonomy = 'services_category';
		$terms = get_terms($taxonomy); // Get all terms of a taxonomy
		if ( $terms && !is_wp_error( $terms ) ) : 
		$all_services =[];
		foreach ( $terms as $term ) :
			$all_services['blocks'].= ".services_blocks.".$term->name.", ";
		endforeach;
		$all_services['blocks'] = substr_replace($all_services['blocks'], "", -2);
		echo "var all_blocks = $('".$all_services['blocks']."');\n";
		foreach ( $terms as $term ) :

			echo "$('.services.".$term->name."').click(function(e) {";
				echo "all_blocks.hide(150); \n";
				echo "if ($('.services_blocks.".$term->name."').css('display') === 'none') { $('.services_blocks.".$term->name."').show('slow'); }";
				echo "$('html, body').animate({scrollTop: $('#services_blocks_cont').offset().top - 80 });";
				echo "e.preventDefault();";
					
			echo "})"."\n";


			
		endforeach;

		endif;
	?>
	
</script>
<script>
	$(document).ready(function() {
        $("#gallery_posts").lightGallery({
			selector: '.item'
		}); 
    }); 
</script>

</body>
</html>
