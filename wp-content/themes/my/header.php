<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<?php 
global $wp;
$perm = home_url( $wp->request );
$exp_es = explode("/", $perm);
$exp_ru = explode("/", $perm);
$perm = explode("/", $perm);
$es = "es";
$ru = "ru";

if($perm[3] === "es" || $perm[3] === "ru") {
	unset($perm[3]); 
}
if($exp_es[3] !== "es") {
	array_splice( $exp_es, 3, 0, $es);
}
if($exp_ru[3] !== "ru") {
	array_splice( $exp_ru, 3, 0, $ru);
}
if ($exp_es[4] === "ru") {
	unset($exp_es[4]);
}
if ($exp_ru[4] === "es") {
	unset($exp_ru[4]);
}
$exp_es = implode("/", $exp_es);
$exp_ru = implode("/", $exp_ru);
$perm = implode("/", $perm);
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js" style="margin-top: 0 !important">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="shortcut icon" type="image/png" href="<?php bloginfo('template_directory');?>/images/icon.ico"/>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<script lang="javascript">
		window.onload = function () { document.getElementById("loader").style.display = "none"; console.log("loaded !!") };</script>
	<style> #loader { text-align: center; background: white; position: fixed; display: block; width: 100%; height: 100%; z-index: 999999999999; }
			#preloadervideo { margin: 0 auto;} </style>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/grid.css" type="text/css">
	<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/flexslider.css" type="text/css">
	<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/master.css" type="text/css">
	<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/font.css" type="text/css">
	<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/lightgallery.css" type="text/css">
	<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/flags.min.css" type="text/css">

	<?php // wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="loader">
	<video id="preloadervideo" width="230" height="230" autoplay loop>
	<source src="<?php bloginfo('template_directory');?>/images/loadimg.mp4" type="video/mp4">
	Page is loading ..
	</video>
	Page is loading ..
</div>
<div id="error_show"></div>
<header id="header">
<div class="row">
	<div class="col-md-3" id="logo">
		<?php if ( function_exists( 'the_custom_logo' ) ) {
    the_custom_logo();
} ?>
	</div>
	<div class="col-md-9" id="menu_top">
		<?php $args = array( 'post_type' => 'acme_product', 'posts_per_page' => 10 );
			$the_query = new WP_Query( $args ); $num_post =1;
			while ( $the_query->have_posts() ) { $the_query->the_post(); ?>
				<?php if($num_post ==2) {?>
				<div class="col-sm-4"> <!-- MENU -->
				<?php if($the_query->current_post==0) : ?><a href="tel:<?php echo get_the_title(); ?>"><?php $count_post++ ?><?php endif; ?>
				<?php if($the_query->current_post==1) : ?><a href="mailto:<?php echo get_the_title(); ?>"><?php $count_post++ ?><?php endif; ?>
				<?php if($the_query->current_post==2) : ?><a href="http://maps.google.com/?q=<?php echo get_the_title(); ?>" target="_blank"><?php $count_post++ ?><?php endif; ?>
				<?php the_post_thumbnail('thumbnail', array('class' => 'menu_thumbnail')); ?>
					<div class="menu_inline">
						<h3><?php echo the_title();?></h3>
						<h6><?php echo the_content();  ?></h6>
					</div>
					</a>
				</div>
				
				<?php } else { ?>
				<div class="col-sm-3">
				<?php if($the_query->current_post==0) : ?><a href="tel:<?php echo get_the_title(); ?>"><?php $count_post++ ?><?php endif; ?>
				<?php if($the_query->current_post==1) : ?><a href="mailto:<?php echo get_the_title(); ?>"><?php $count_post++ ?><?php endif; ?>
				<?php if($the_query->current_post==2) : ?><a href="http://maps.google.com/?q=<?php echo get_the_title(); ?>" target="_blank"><?php $count_post++ ?><?php endif; ?>
				<?php the_post_thumbnail('thumbnail', array('class' => 'menu_thumbnail')); ?>
				<div class="menu_inline">
					<h3><?php echo the_title();?></h3>
					<h6><?php echo the_content();  ?></h6>
					</a>
				</div>
				</div>
				
				<?php } $num_post++; ?>
		<?php } wp_reset_postdata(); ?>
		<div class="col-sm-2" id="button_menu_top">
			<p></p>
		</div>
	</div>
	
</div>
<div class="conteiner-fluid" id="menu_source">
	<div class="first main">
		<?php _e("We services from start to finish");?>
	</div>
	<?php if (in_array('page-id-158', get_body_class())) : ?>
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
			<div class="second main">
				<?php _e("Go back to home page");?>
			</div>
		</a>
		</div>
	<?php else : ?>
		<a href="<?php echo esc_url( get_permalink(158) ); ?>">
			<div class="second main">
				<?php _e("View our previous projects");?>
			</div>
		</a>
	</div>
	<div class="gallery_bubble">
		<h5> <?php _e("Take a look on our gallery!<br/>"); ?></h5>
		<img src="<?php bloginfo('template_directory');?>/images/gallery.png"/>
	</div>
	<?php endif; ?>

</header>
<div id="languages">
	<a class="flag flag-gb" href="<?php echo $perm."/"; ?>"></a>
	<a class="flag flag-es" href="<?php echo $exp_es."/"; ?>"></a>
	<a class="flag flag-ru" href="<?php echo $exp_ru."/"; ?>"></a>
</div>
		
	
<div id="content" class="site-content">
