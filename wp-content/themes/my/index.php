<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<?php $argss = array( 'post_type' => 'gallery', 'posts_per_page' => -1 );
	$the_queryy = new WP_Query( $argss ); ?>
	<div class="flexslider">
		<ul class="slides">
		<?php
		if ( $the_queryy->have_posts() ) :
			while ( $the_queryy->have_posts() ) : $the_queryy->the_post(); ?>
				<li style="background-image: url(<?php the_post_thumbnail_url(); ?>);  background-size: 50% auto;">
					<div class="slide_text1"><?php echo get_the_content(); ?></div>
				</li><?php
			endwhile;
		else :
			echo wpautop( 'Sorry, no posts were found' );
		endif;
		?>
				
	
		</ul>
	</div>
	<?php  wp_reset_postdata(); ?>
<!--
	// partners tab
<div class="container-fluid" id="partners">
	 <div class="row">
		<?php $arg = array( 'post_type' => 'partners', 'posts_per_page' => 8 );
		$the_quer = new WP_Query( $arg ); ?>
		<section class="customer-wide slider">
			<?php
			if ( $the_quer->have_posts() ) :
				while ( $the_quer->have_posts() ) : $the_quer->the_post(); ?>
					<div class="slide"><img src="<?php the_post_thumbnail_url(); ?>"></div>
					<?php
				endwhile;
			else :
				echo wpautop( 'Sorry, no posts were found' );
			endif;
			?>
		</section>
	</div>
	<?php  wp_reset_postdata(); ?>
		
</div>
-->

<?php // INDEX ?>
<div class="container-fluid padding-top-high" id="intro">
	<div class="row"> 
		<div class="animated col-sm-6 col-md-3 padding-top-small padding-bottom-small">
			<div class="padding awsome_div">
			<div ><i class="demo-icon fa">&#xe800;</i></div>
				<h1>Professionla</h1>
				<p>A team of incomparable specialists</p>
			</div>
		</div>
		<div class="animated col-sm-6 col-md-3 padding-top-small padding-bottom-small">
			<div class="padding awsome_div">
				<div><i class="demo-icon fa">&#xe801;</i></div>
				<h1>Professionla</h1>
				<p>A team of incomparable specialists</p>
			</div>
		</div>
		<div class="animated col-sm-6 col-md-3 padding-top-small padding-bottom-small">
			<div class="padding awsome_div">
				<div><i class="demo-icon fa">&#xe802;</i></div>
				<h1>Professionla</h1>
				<p>A team of incomparable specialists</p>
			</div>
		</div>
		<div class="animated col-sm-6 col-md-3 padding-top-small padding-bottom">
			<div class="padding awsome_div">
				<div><i class="demo-icon fa">&#xe803;</i></div>
				<h1>Professionla what whta</h1>
				<p>A team of incomparable specialists dasdiim iaimim asidjai j aisjd ijasidj asd jaisj diaj</p>
			</div>
		</div>
	</div>
</div>
<?php // SERVICES ?>
<div class="container-fluid padding-bottom" id="services">
	<div class="row padding-bottom padding-top" id="title">
		<h1 class="title_h1 padding-bottom">
		<?php $query = new WP_Query( array('p' => 72, 'post_type' => 'custom_post') ); $query->the_post(); ?>
			<?php the_title(); ?>
		 
		</h1>
	</div>
	<div class="row padding-bottom" id="services_p">
		<?php the_content(); ?>
	</div>

	<?php wp_reset_postdata(); ?>

	<div class="row" id="services_category_row">
		<?php
			$taxonomy = 'services_category';
			$app_str_cat = [];
			$terms = get_terms($taxonomy); // Get all terms of a taxonomy
		?>
		<?php $fb = $terms[0]->name; ?>
		<?php if ( $terms && !is_wp_error( $terms ) ) : ?>
			<?php foreach ( $terms as $term ) : ?>
				<div class="col-sm-6 col-md-4">
					<div class="services <?php echo $term->name ?> <?php if( $fb == $term->name) { ?> active_service <?php } ?>">
						<?php 
						if (! empty($term->description )) :
							echo '<img src="'.$term->description.'"/>';
						endif;
						echo " <h3>".$term->name."</h3>"; 
						?> 
					</div>
				</div>
			<?php endforeach; ?>
				
		<?php endif; ?>
	</div>
	<div class="row" id="services_blocks_cont"> <?php // here is the content for gallery site ?>
	
	<?php foreach ( $terms as $term ) { ?>
	<?php 
		$args = array('post_type' => 'services',
			'tax_query' => array(
				array(
					'taxonomy' => $taxonomy,
					'field' => 'slug',
					'terms' => $term->slug,
				),
			),
		);
		$the_query = new WP_Query( $args ); $nt =1;
		if ( $the_query->have_posts() ) :
			while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

	<div class="services_blocks <?php echo $term->name ?>" <?php if( $fb != $term->name) { ?>style="display:none" <?php } ?>>
				
					 <?php $nt++; echo the_content() ?>
				</div>
		<?php endwhile;  endif; // end of the loop. ?>
		<?php wp_reset_postdata(); ?>
		<?php } ?>
	</div>
</div>

<?php // ABOUT US ?>
<?php query_posts('p=110&post_type=custom_post'); ?>
	<?php  while ( have_posts() ) : the_post(); ?>
<div class="container-fluid padding-bottom-high padding-top-high bg-lin-red" id="aboutus">
	<div class="row">
		<div class="col-md-6">
			<?php the_post_thumbnail(); ?>
		</div>
		<div class="col-md-6 padding-bottom" id="content_a">
			<h1 class="title_h1 padding-bottom underline_white">
				<?php the_title(); ?>
			</h1>
			<?php the_content(); ?>
		</div>
	</div>
	<?php endwhile; ?>
</div>
<?php wp_reset_query(); ?>


<?php get_footer(); ?>
<!--
<a href="<?php echo get_term_link($term->slug, $taxonomy); ?>">
-->