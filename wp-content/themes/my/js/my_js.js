$(window).on('load', function() {
    $('.flexslider').flexslider({
        animation: "slide",
    });
});
$('.customer-wide').slick({
    speed: 400,
    autoplay: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });

// height slider top
$(window).on("load resize ", function(e){
    $('ul.slides li').css({"height":$(window).height()});
})


// height header-top
$('.header-top').height($('header').height()+40);

$(window).scroll(function() {
    //var menu_top = $('#menu_top');
    var header = $('#header');
    var top = $(window).scrollTop();
    if ($(window).width() > 996) {
        if (top == 0) {
            //menu_top.removeClass("sticky");
                $("#header h6").fadeIn("slow");
                header.removeClass("bounce_menu");
                $(".gallery_bubble").css("top", 200);
                $("#logo img").css({"max-height":"110px", "margin-top":"-20px"});
            }
        
        else {
            // menu_top.addClass("sticky");
                $("#header h6").hide();
                $(".gallery_bubble").css("top", 160);
                header.addClass("bounce_menu");
                $("#logo img").css({"max-height":"82px", "margin-top":"0px"});
            }
    }
    $('ul.slides li').css({
        "opacity": function() {
        var elementHeight = $(this).height();
        return 1- top/ elementHeight;}
        })
    });
    $('ul.slides li').css({
        'background-position-y': top/3.5
    })
$('#button_menu_top, #menu_close').click(function() {
        if($(window).width() > 762) {
            $('#menu_contact *').toggle();
            $('#menu_contact').toggleClass("display_menu");
            if($('#error_show').css("display") == "block") {
                $('#error_show').css('display', 'none');
            }
        }
        else {
            $('header').toggleClass('header_pop');
        }
    });

// first word underlined

$('.services_blocks figure.gallery-item:nth-child(3n)').after('<div class="clear_both padding_both_big"> </div>');
$('.title_h1').each(function(index) {
    var firstWord = $(this).text().split(' ')[0];
    var replaceWord = "<span class='title_first'>" + firstWord + "</span>";
    var newString = $(this).html().replace(firstWord, replaceWord);
    $(this).html(newString);
});
$('#services_category_row .services').click(function() {
    $("#services_category_row").find('.active_service').removeClass('active_service');
    $(this).first().addClass("active_service");
})

// scroll to top of div

function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}
// If element is scrolled into view, fade it in
$('.animated').css({"display":"none"})
$(window).scroll(function() {
    $('.animated').each(function() {
        $(this).addClass('zoomInDown').css({"display":"block"});
    });
});
if($('.wpcf7-validation-errors,  .wpcf7-spam-blocked').css('display') == 'none') {
    $('#error_show').css({"display":"block"});
    $('#menu_contact *').toggle();
    $("#menu_contact").toggleClass("display_menu");
    $('.wpcf7-validation-errors, .wpcf7-spam-blocked').detach().appendTo('#error_show');
}

// add class to img lightGallery plug

$('#gallery_posts .content img').wrap(function () {
    var th = $(this).attr("src");
    $(this).wrap('<div class="item" data-src="'+th+'"></div>')
})


// buble to show gallery

let bubble = document.querySelector(".gallery_bubble");
if(bubble) {
    let sec_head = document.querySelectorAll(".main.second, .gallery_bubble");
    let w_991 = window.innerWidth > 991;
    let expire_t = 1000*60*60*14; //minut now
    if (document.cookie.indexOf("visited=") >= 0) {
    }
    else {
        expiry = new Date();
        expiry.setTime(expiry.getTime()+(expire_t));
        document.cookie = "visited=true; expires=" + expiry.toGMTString();
        if (w_991) {
            setTimeout(function(){
                bubble.style.cssText = "opacity: 1; max-height: 100%; transition: all 1s;";
        
            }, 5000)
        }
        
    }
    for(let i = 0; i < sec_head.length; i++) {
        sec_head[i].addEventListener('mouseover', function(event) {
            bubble.style.opacity = 0;
            bubble.style.maxHeight = "0%" ;
            bubble.style.visibility ="hidden";
        })
    }
}
else {
    console.log("no bubble");
}